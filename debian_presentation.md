# About Me

* [@jorgescalona](https://jorgescalona.github.io/)

Code over UNIX

Diferencia entre compilado e instalado (adaptación al HW)

# Ninja Dev Enviroment

CI/CD   (set of tools testing and deploy)
erick raymond [Como hacer preguntas inteligentes](http://www.unirioja.es/cu/arjaime/ParticiparEnForos.pdf)

# OpenSource POWA   ------------------->    Debian Style

* Free Knwoledge (Conocimiento libre)
    
1. [Debian](https://www.debian.org/index.es.html)
1. Gnu/Linux (github)
1. [Buen conocer y Flok Society.](http://floksociety.org/)

* Best Practices & Guidelines.

1. [Python PEP8](https://www.python.org/dev/peps/pep-0008/)
1. [Odoo 10 Guidelines](https://www.odoo.com/documentation/10.0/reference/guidelines.html)

* Empaquetado
* Versionado y CVS

1. rama o nombre clave
1. Rude testing

* QA

1. [viertualenv](https://docs.python-guide.org/dev/virtualenvs/)
1. [Docker](https://www.docker.com/)
1. [Kubernets](https://kubernetes.io/)

# Tools.

* [Text Editor VI POWA!](https://vim.spf13.com/)
* [Shell Multiplexing!](https://github.com/tmux/tmux/wiki)
* [CVS and GIT](https://git-scm.com/book/es/v2)
    
1. [gitlab](https://gitlab.com/)

* Isolation Enviroment
    
1. viertualenv
1. Docker
1. Kubernets

* [Documentation](https://realpython.com/documenting-python-code/)
* Debug tools [Python PDB](https://docs.python.org/3/library/pdb.html)

# Formation

* Oficial Documentation The BEST!
* Code imerssion

# Dev Methodology

* TDD    Obey the Goat!

# Conclusions

* No reinventar la rueda.
* [Copiad Malditos](https://www.youtube.com/watch?v=YY0i4xJss9c)
* OpenSource y Libertad. Rompe el paradigma!
* Educación Conductista y Prusiana. [Pedagogía de la liberación Paulo Freire.](https://es.wikipedia.org/wiki/Pedagog%C3%ADa_de_la_liberaci%C3%B3n)
* Microsoft y su aporte a Gnu/Linux. 4 libertades o muere!
* Otros casos: Google + Pentágono / [BraveBrowser](https://es.wikipedia.org/wiki/Brave_(navegador_web)). (creador de javascript y cofundador de Moziila Brendan Eich.)
* Etica y Libertad!
